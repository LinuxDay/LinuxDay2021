# Appunti Linux Day

## Introduzione

- Ci sono circa 250 linguaggi di programmazione. Perché non ce n’è soltanto uno, ma fatto bene?
- Domande al pubblico, (*) = da fare, le altre opzionali:
    - (*)Cosa significa per voi "linguaggio di programmazione"?
    - (*)Che linguaggi conoscete?
    - (*)Quali vi hanno interessato maggiormente?
    - Vi siete documentarvi per conto vostro per svolgere progetti personali?
    - Pensate che questi linguaggi siano utili per il futuro (lavorativo)?
- Perché nasce un linguaggio di programmazione? (qui si spiega, di fatto, che cos'è un linguaggio di programmazione):
    - Impartire gli ordini ad una macchina
    - Astrazione

------ spostare dopo ------
- Motivi di successo dei linguaggi, effetto volano:
    - Dovrei imparare il linguaggio più popolare perché è più facile trovare lavoro, informazioni, librerie.
    - E se ormai ho già imparato quel linguaggio, chi me lo fa fare di impararne un altro?
- Il linguaggio si adatta ai gusti degli utenti, alle esigenze e limitazioni tecniche del momento, agli obiettivi pratici. Esempio: perché al ristorante non vendono solo la pizza margherita?
---------------------------

## Breve storia dei linguaggi di programmazione

- "Programmazione" meccanica pre-era dell'informazione: il telaio Jaquard, Babbage, Ada Lovelace e la storiellina del governo inglese.
- Holleritz e la nascita di IBM (applicazione dei principi delle schede perforate alle calcolatrici elettromeccaniche).
- Periodo dei computer a valvole: vera programmazione in assembly (eventualmente, speedcoding come primo tentativo di astrazione).
- Tavolina degli mnemonici del 6502 o equivalente, considerazioni sulle limitazioni date dalle mancanze delle istruzioni macchina.
- Necessità di linguaggi più vicini alla lingua parlata: il Fortran e il COBOL, (cenno a Grace Hopper); [il COBOL è in voga ancora oggi](https://www.pcmag.com/news/us-states-ask-for-help-from-cobol-programmers);
- Evoluzione del computer da calcolatore per le grandi università a soprammobile da ufficio;
- Programmazione "meccanica", computer senza dispositivi "grafici" di input. Da binario agli mnemonici dell'assembly.
- I programmini in assembly dell’apollo 11 (burn baby burn);
- primo esempio di come avviene un fork dei linguaggi;
- L’APL (A Program Language), capostipite di molti linguaggi moderni. Quello con la tastiera astrusa con un esempio;
- Slide tutta nera: questo è l’output su schermo prodotto dai linguaggi descritti finora. Il calcolatore non aveva il video, arriva dopo.
- Introduzione della print, “si stampa a schermo” un hello world con vice e il basic del C64.
- Fine anni 60, invenzione di C e UNIX.
- Motivi per cui i linguaggi di programmazione si dividono in filoni:
    - Obsolescenza
    - Motivi commerciali (imposizione da aziende)
    - Paradigmi di programmazione:
        - Imperativi
            - Procedurali, strutturati, OOP
            - Esempio del linguaggio OO: Definizione del tipo (rappresentazione del dato e operazioni), esempio con il tipo “vettore”: con una sola variabile mi porto dietro l’informazione “mascherata” e le operazioni ad esso correlate.
            - Interazione: proviamo a scrivere il programma senza classi, in C.
        - Dichiarativi
            - funzionali o logic programming. Esempio di Excel come linguaggio funzionale.
        - Linguaggi distribuiti in concorrenza: su processori multicore si può fare l’esempio dei due pizzaioli con un solo forno.
    - Un linguaggio ha successo anche quando puoi usare un sottoinsieme di cose slegate da tutto il resto del linguaggio. Se un linguaggio è fatto da una pila di caratteristiche è meglio perché così non bisogna studiarsi tutto il malloppo per fare qualcosa. (subset fallacy).
- Carrellata dei linguaggi esoterici
    - Linguaggio esoterico per instagram coi pixel che corrispondono a istruzioni
    - brainfuck
    - Moo MOO mOO mOo
    - Monicelli

## La programmazione in ambiente WEB
- 10 slides in 10 minuti.

## Cose rimaste fuori
- Esempio delle ricette: un linguaggio di programmazione è la ricetta, un linguaggio di descrizione o markup sono gli ingredienti.


- Non è detto che i linguaggi di oggi siano adatti alle applicazioni richieste: 