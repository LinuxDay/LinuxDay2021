# Gli editor di codice (di testo puro!)

## Un minimo di contestualizzazione storica

### Le schede perforate

Come abbiamo visto uno dei primi metodi \"evoluti\" di scrittura del
codice sono state le shcede perforate. Ogni secheda perforata
corrispondeva ad una riga di codice. Se si voleva modifcare una riga si
buttava la scheda e se ne perforava una nuova

### Le telescriventi

Con l\'avvento delle telescriventi nascono i primi editor. La tecnologia
non consente cose particolarmente evolute. Per semplificare si
utilizzano modalità di funzionamento separata e alternative:

Modalità comando
:   in questa modalità si impartiscono i comandi all\'editor.

Modalità inserimento
:   questa è la modalità in cui si inserisce il testo.

La necessità di ottimizzare l\'uso delle telescriventi rende i programmi
di editing particolarmente avari/criptici nell\'interazione con
l\'operatore. Nasce qui la definizione di \"editor di linea\" perché
usando la telescrivente si opera su di una riga per volta, e non si può
tornare indietro mentre si inseriscono righe.

### I terminali a caratteri

Sono un grande passo in avanti rispetto alle telescriventi. Tuttavia le
prime versioni sono ancora molto limitate. Gli editor di testo
mantengono inizialmente la stessa impostazione usata per le
telescriventi; gli editor di riga sono ancora ampiamente utilizzati, ma
ben presto iniziano ad uscire i primi editor \"visuali\". \"ED\" di cui
vedremo una breve dimostrazione di funzionamento appartiene alla
famiglia degli editor di riga.

### Evoluzione dei terminali a caratteri e avvento degli editor \"a tutto schermo\"

Hanno, rispetto agli editor di riga, la capacità di poter \"navigare\"
nel testo in modo molto più semplice ed intuitivo rispetto agli editor
di riga. Negli editor visuali il testo viene mostrato sullo schermo del
terminale ed è possibile spostarsi al suo interno usando i tasti di
spostamento del cursore. Con l\'avvento degli editor visuali gli editor
di riga cadono ben presto in disuso.

### L\'era moderna: le GUI (graphic user interface) e gli ambienti \"visual\"

Il passaggio dai computer con interfaccia utente esclusivamente a
carattere a quella grafica è stata una rivoluzione, anche se
l\'interazione con il computer tramite interfaccia a caratteri ha
provato di essere spesso molto più efficiente rispetto all\'interazione
tramite interfaccia grafica. Gli ambienti di sviluppo si sono evoluti
consentendo la crazione grafica di queste interfacce; dal punto di vista
della stesura del codice i concetti fondamentali sono rimasti quelli
dell\'era \"terminali a caratteri\".

## Breve dimostrazione dell\'uso di ED

All\'apertura mostra poche scarne informazioni: se si sta creando un
nuovo file mostra un messaggio che assomiglia ad un errore, se si sta
aprendo un file esistente mostra quanti bytes il file occupa in memoria.

Per vedere il contenuto del file si devono usare i comandi \"p\" o
\"n\".

Per posizionarsi su di una riga si inserisce il numero di riga

Per sapere il numero di riga corrente si usa il comando \".=\"

Per inserire \"prima\" della riga corrente si usa il comando \"i\"
(insert); con questo comando si passa dalla modalità comando alla
modalità inserimento.

Per inserire \"dopo\" la riga corrente si usa il comando \"a\" (append);
con questo comando si passa dalla modalità comando alla modalità
inserimento.

Per sostituire completamente il contenuto della riga corrente si usa il
comando \"c\" (change). Sostituisce la riga corrente con le righe che si
andranno ad inserire (possono essere più di una riga). Con questo
comando si passa dalla modalità comando alla modalità inserimento.

Per uscire dalla modalità inserimento e tornare alla modalità comando si
usa il carattere punto (\".\") che deve essere l\'unico carattere
all\'inizio di una nuova riga.

Esistono poi dei comandi \"avanzati\" che consentono la ricerca (e
sostituzione) all\'interno del testo usando un linguaggio particolare
(potente e criptico) chiamato \"regular expression\", che per essere
affrontato necessiterebbe di un incontro specifico.

## Approfondimenti

Se qualcuno fosse interessato ad approfondire l\'evoluzione degli
strumenti di sviluppo, oltre che migliorarne la conoscenza e la capacità
di utilizzo avanzato, può venire a trovarci al GOLEM ogni martedì sera.
